﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Printer
    {
        public void Print (string text)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
    class PrinterYellow:Printer
    {
        public new void Print (string text)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string text;
            Console.WriteLine("Введите текст");
            text = Console.ReadLine();

            
            PrinterYellow instance = new PrinterYellow();
            instance.Print(text);

            //upcast
            Printer instanceup = instance;
            instanceup.Print(text);

            //downcast
            PrinterYellow instancedown = (PrinterYellow)instanceup;
            instancedown.Print(text);

                Console.ReadKey();
        }
    }
}
